(in-package :cl-user)
(defpackage myapp-test-asd
  (:use :cl :asdf))
(in-package :myapp-test-asd)

(defsystem myapp-test
  :author "luoxing"
  :license ""
  :depends-on (:myapp
               :prove)
  :components ((:module "t"
                :components
                ((:file "myapp"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
